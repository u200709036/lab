public class Point {
	private int xCord;
	private int yCord;

	public Point(int xCord, int yCord) {
		this.xCord = xCord;
		this.yCord = yCord;

	}

	public int getxCord() {
		return xCord;
	}

	public int getyCord() {
		return yCord;
	}

	public double distanceFromPoint(Point point){
		int xDiff = xCord - point.xCord;
		int yDiff = yCord - point.yCord;

		return Math.sqrt(xDiff * xDiff + yDiff * yDiff);

	}
}
