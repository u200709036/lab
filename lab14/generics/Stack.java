package generics;

import java.util.List;

public interface Stack<T extends Number> {


    public void push(T item);

    T pop();

    public boolean empty();

    List<T> toList();

    default void addAll(Stack<? extends T> aStack) {
        List<? extends T> list = aStack.toList();
        for (T item : list){
            push(item);
        }
    }
}
