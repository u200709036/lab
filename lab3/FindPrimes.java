public class FindPrimes {
    public static void main(String[] args) {
        int max = Integer.parseInt(args[0]);

        //Print the numbers less than max
        //For each number less than max
        for(int number = 2; number < max; number++) {
            //Let divisor = 2;
            int divisor = 2;
            //Let isprime = true;
            boolean isprime = true;
            //while divisor less than number
            while(divisor < number && isprime) {
                //if the number is divisible by divisor
                if(number % divisor == 0)
                    isprime = false; //isprime = false;
                //increment divisor
                divisor++;
            }
            //If the number is prime
            if (isprime)
                System.out.print(number + " ");//print the number
        }
    }
}