public class MyDate {

    int day, month, year;

    int[] maxDays = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31 ,30, 31};

    public MyDate(int day,int month, int year){
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public String toString(){
        return year + "-" +  (month < 10 ? "0": "") + month + "-" + (day < 10 ? "0": "") + day;
    }

    public void incrementDay() {
        day++;
        if(day > maxDays[month-1]){
            day = 1;
            incrementMonth();
        }else if (month == 2 && day == 29 && !inLeapYear()){
            day = 1;
            incrementMonth();
        }
    }

    public boolean inLeapYear() {
        return  year % 4 == 0;
    }

    public void incrementYear(int diff) {
        year += diff;
        if (month == 2 && day == 29 && !inLeapYear()){
            day = 28;
        }
    }

    public void decrementDay() {
        day--;
        if(day == 0) {
            decrementMonth();
            if (!inLeapYear() && month == 2){
                day = 28;
            }else {
                day = maxDays[month - 1];
            }
        }

    }

    public void decrementYear() {
        incrementYear(-1);
    }

    public void decrementMonth() {
        month--;
    }

    public void incrementDay(int diff) {
        while( diff > 0) {
            incrementDay();
            diff--;
        }
    }

    public void decrementMonth(int diff) {
        incrementMonth(-diff);
    }

    public void incrementMonth(int diff) {
        month += diff;
        int yearDiff = (month-1) / 12;

        int newMonth = ((month-1) % 12) +1;

        if (newMonth < 0)
            yearDiff--;
        year += yearDiff;

        month = newMonth < 0 ? newMonth + 12 : newMonth;

        if (day > maxDays[month-1]) {
            day = maxDays[month-1];
            if (month == 2 && day == 29 && !inLeapYear())
                day = 28;
        }
    }

    public void decrementDay(int diff) {
        while (diff > 0) {
            decrementDay();
            diff--;
        }
    }


    public void decrementYear(int diff) {
        incrementYear(-diff);
    }

    public void incrementMonth() {
        incrementMonth(1);
    }

    public void incrementYear() {
        incrementYear(1);
    }

    public boolean isBefore(MyDate anotherDate) {
        if (year > anotherDate.year) {
            return false;
        }else if (year == anotherDate.year && month > anotherDate.month) {
            return false;
        }else if (year == anotherDate.year && month == anotherDate.month && day > anotherDate.day) {
            return false;
        }
        return true;
    }

    public boolean isAfter(MyDate anotherDate) {
        if (year > anotherDate.year) {
            return true;
        }else if (year == anotherDate.year && month > anotherDate.month) {
            return true;
        }else if (year == anotherDate.year && month == anotherDate.month && day > anotherDate.day) {
            return true;
        }
        return false;
    }

    public int dayDifference(MyDate anotherDate) {
        int diff = 0;

        if (isAfter(anotherDate)){
            if (year % anotherDate.year == 1) {
                for (int a = anotherDate.month; a <= 11; a++){
                    diff += maxDays[a];
                }
                for (int i = 1; i < month-1; i++){
                    diff += maxDays[i-1];
                }
                diff += anotherDate.day;
                diff += day;

                return diff;

            }
            else if (year % anotherDate.year >= 2) {
                diff += (year % anotherDate.year) * 365;
                for (int a = anotherDate.month; a <= 11; a++){
                    diff += maxDays[a];
                }
                for (int i = 1; i < month-1; i++){
                    diff += maxDays[i-1];
                }
                diff += anotherDate.day;
                diff += day;
                diff -= 365;

                return diff;

            }else {
                if (month >= anotherDate.month && !inLeapYear()) {
                    maxDays[1] = 28;
                    for (int a = anotherDate.month; a < month; a++) {
                        diff += maxDays[a - 1];
                    }
                    if (day == 1) {
                        diff += day - 1;
                    }else {
                        diff += day;
                    }
                    diff -= anotherDate.day;

                    return diff;

                }else if (month >= anotherDate.month) {
                    for (int i = anotherDate.month; i < month; i++) {
                        diff += maxDays[i - 1];
                    }
                    if (day == 1) {
                        diff += day - 1;
                    }else {
                        diff += day;
                    }
                    diff -= anotherDate.day;

                    return diff;
                }else {
                    return day - anotherDate.day;
                }

            }


        }else if (isBefore(anotherDate)){
            if (anotherDate.year % year == 1) {
                for (int a = month; a <= 11; a++){
                    diff += maxDays[a];
                }
                for (int i = 1; i < anotherDate.month-1; i++){
                    diff += maxDays[i-1];
                }
                diff += anotherDate.day;
                diff += day;

                return diff;

            }
            else if (anotherDate.year % year >= 2) {
                diff += (anotherDate.year % year) * 365;
                for (int a = month; a <= 11; a++){
                    diff += maxDays[a];
                }
                for (int i = 1; i < anotherDate.month-1; i++){
                    diff += maxDays[i-1];
                }
                diff += anotherDate.day;
                diff += day;
                diff -= 365;

                return diff;

            }else {
                if (anotherDate.month % month >= 1 && !inLeapYear()) {
                    maxDays[1] = 28;
                    for (int a = month; a < anotherDate.month; a++) {
                        diff += maxDays[a - 1];
                    }
                    if (anotherDate.day == 1) {
                        diff += anotherDate.day - 1;
                    }else {
                        diff += anotherDate.day;
                    }
                    diff -= day;

                    return diff;

                }else if (anotherDate.month % month >= 1) {
                    for (int i = month; i < anotherDate.month ; i++) {
                        diff += maxDays[i - 1];
                    }
                    if (anotherDate.day == 1) {
                        diff += anotherDate.day - 1;
                    }else {
                        diff += anotherDate.day;
                    }
                    diff -= day;

                    return diff;
                }else {
                    return day - anotherDate.day;
                }

            }



        }

        return diff;
    }
}